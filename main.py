#!/usr/bin/python
# -*- coding: utf-8 -*-

from flask import Flask, render_template, request, abort
from modules.main_handler import MainHandler, response_create

import json


app = Flask(__name__)
main_handler = MainHandler()


@app.route("/")
def main():
    return render_template('index.html', page_title="Nginx Dashboard")


@app.route("/data", methods=["GET", "POST"])
def data():
    if request.method == "GET":
        return main_handler.serve_statistic_data()
    elif request.method == "POST":
        try:
            operation = request.json["operation"]
            if operation == "enable":
                return main_handler.enable_us_server(
                    request.json["servers"],
                    request.json["upstream"]
                )
            elif operation == "disable":
                return main_handler.disable_us_server(
                    request.json["servers"],
                    request.json["upstream"]
                )
            elif operation == "modify":
                return main_handler.modify_us_server(
                    request.json["servers"].split(","),
                    request.json["upstream"],
                    request.json["weight"],
                    request.json["max_fails"],
                    request.json["fail_timeout"],
                    request.json["max_conns"],
                    request.json["server_status"]
                )
            elif operation == "add_new":
                return main_handler.add_us_server(
                    request.json["ip"],
                    request.json["port"],
                    request.json["upstream"],
                    request.json["weight"],
                    request.json["max_fails"],
                    request.json["fail_timeout"],
                    request.json["max_conns"],
                    request.json["server_status"]
                )
        except KeyError as e:
            abort(400, "Argument is missing: " + str(e))
        except Exception as e:
            abort(400, "Invalid argument: " + str(e))


@app.errorhandler(404)
def page_not_found(e):
    return response_create(json.dumps({"STATUS": "error", "ERROR": str(e)})), 404


@app.errorhandler(400)
def bad_request(e):
    return response_create(json.dumps({"STATUS": "error", "ERROR": str(e)})), 400


@app.errorhandler(401)
def unauthorized_request(e):
    return response_create(json.dumps({"STATUS": "error", "ERROR": str(e)})), 401


@app.errorhandler(405)
def unauthorized_login(e):
    return response_create(json.dumps({"STATUS": "error", "ERROR": str(e)})), 405


@app.errorhandler(500)
def server_error(e):
    return response_create(json.dumps({"STATUS": "error", "ERROR": str(e)})), 500


if __name__ == '__main__':
    app.run(port=5000, debug=True)
