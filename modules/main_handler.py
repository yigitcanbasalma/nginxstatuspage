#!/usr/bin/python
# -*- coding: utf-8 -*-

from functools import wraps
from flask import Response, abort

import requests
import json
import os


def response_create(data, rtype="json"):
    avail_rsp = {
        "json": "application/json"
    }
    return Response(data, mimetype=avail_rsp[rtype])


def dynamic_us(f):
    @wraps(f)
    def decorated_function(self, *args, **kwargs):
        if not self.cfg["dynamic_upstream"]:
            abort(404, "Dynamic upstream module is disabled.")
        return f(self, *args, **kwargs)

    return decorated_function


class MainHandler(object):
    def __init__(self):
        self.cfg = None
        self.responser_ip = None
        self.load_config(os.getcwd() + "/modules/config.json")

    def load_config(self, file_path):
        with open(file_path, "r") as config:
            self.cfg = json.load(config)

    @staticmethod
    def response_create(data, rtype="json"):
        avail_rsp = {
            "json": "application/json"
        }
        return Response(data, mimetype=avail_rsp[rtype])

    @staticmethod
    def requester(url, headers=None, data=None, auth=None, r_type="text", method="GET", timeout=5, response_headers=False):
        try:
            avail_methods = {
                "GET": requests.get,
                "POST": requests.post
            }
            if headers is None:
                headers = {
                    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:10.0) Gecko/20100101 Firefox/10.0 (Nginx Load Balancer Dashboard Tools)"
                }
            data = data if data is not None else dict()
            r = avail_methods[method](url, headers=headers, verify=False, json=data, auth=auth, timeout=timeout)
            r_headers = ["{0}: {1}".format(k, v) for k, v in r.headers.iteritems()] if response_headers else []
            if r_type == "json":
                return r.status_code, r.json(), r_headers
            return r.status_code, r.text, r_headers
        except Exception as e:
            return -1, e, []

    def get_backup_node(self):
        for i in self.cfg["server_pool"]["backup"]:
            yield i

    def get_data(self, host, point):
        self.responser_ip = host
        r = self.requester("http://{0}{1}".format(host, point), r_type="json")
        if r[0] == 200:
            return r[1]
        for i in self.get_backup_node():
            return self.get_data(i, point)

    def send_request_all_server(self, port, point, payload):
        headers = {
            "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:10.0) Gecko/20100101 Firefox/10.0 (Nginx Load Balancer Dashboard Tools)",
            "Content-Type": "application/json"
        }
        servers = [i for i in self.cfg["server_pool"]["backup"]]
        servers.append(self.cfg["server_pool"]["master"])
        for i in servers:
            s_code, out, _ = self.requester("http://" + i + ":" + str(port) + point, method="POST", data=payload, r_type="json", headers=headers)
            if s_code != 200 and i == self.responser_ip:
                return out["ERROR"]

    def prepare_data(self):
        try:
            formatted_data = dict()
            for point in self.cfg["status_endpoints"]:
                for k, v in self.get_data(self.cfg["server_pool"]["master"], point).iteritems():
                    if k not in formatted_data:
                        formatted_data[k] = v
                    else:
                        if k == "connections":
                            for e, s in v.iteritems():
                                formatted_data[k][e] += s
            formatted_data["state"] = "Master" if self.cfg["server_pool"]["master"] == self.responser_ip else "Backup"
            if self.cfg["sts_include"]:
                stream_addr = formatted_data["streamServerZones"].keys()
                for addr in stream_addr:
                    formatted_data["streamServerZones"][addr]["requestMsecs"] = formatted_data["streamServerZones"][addr]["sessionMsecs"]
                    formatted_data["streamServerZones"][addr]["requestMsec"] = formatted_data["streamServerZones"][addr]["sessionMsec"]
                    del formatted_data["streamServerZones"][addr]["sessionMsecs"]
                    del formatted_data["streamServerZones"][addr]["sessionMsec"]
                    if addr not in ["*"]:
                        _, port, ip = addr.split(":")
                        new_name = self.cfg["stream_server_replace"]["{0}:{1}".format(ip, port)]
                        formatted_data["streamServerZones"][new_name] = formatted_data["streamServerZones"][addr]
                        formatted_data["streamServerZones"][new_name]["address"] = ip
                        del formatted_data["streamServerZones"][addr]
            return formatted_data
        except AttributeError:
            return False

    def serve_statistic_data(self):
        data = self.prepare_data()
        if not data:
            return self.response_create(json.dumps({"STATUS": "error", "ERROR": "No data found."}))
        return self.response_create(json.dumps({"STATUS": "OK", "MESSAGE": data}))

    @dynamic_us
    def enable_us_server(self, servers, us_name):
        payload = {
            "upstream": us_name,
            "operation": "modify",
            "type": "server",
            "host": None,
            "port": None,
            "changes": {
                "up": True
            }
        }
        state = None
        for server in servers:
            addr, port = server.split(":")
            payload["host"] = addr
            payload["port"] = int(port)
            d = self.send_request_all_server(self.cfg["upstream_control_port"], self.cfg["upstream_control_endpoint"], payload)
            if d is not None:
                state = d
        if state is not None:
            return self.response_create(json.dumps({"STATUS": "error", "ERROR": state}))
        return self.response_create(json.dumps({"STATUS": "OK", "MESSAGE": "Server(s) is enabled."}))

    @dynamic_us
    def disable_us_server(self, servers, us_name):
        payload = {
            "upstream": us_name,
            "operation": "modify",
            "type": "server",
            "host": None,
            "port": None,
            "changes": {
                "down": True
            }
        }
        state = None
        for server in servers:
            addr, port = server.split(":")
            payload["host"] = addr
            payload["port"] = int(port)
            d = self.send_request_all_server(self.cfg["upstream_control_port"], self.cfg["upstream_control_endpoint"], payload)
            if d is not None:
                state = d
        if state is not None:
            return self.response_create(json.dumps({"STATUS": "error", "ERROR": state}))
        return self.response_create(json.dumps({"STATUS": "OK", "MESSAGE": "Server(s) is disabled."}))

    @dynamic_us
    def modify_us_server(self, servers, us_name, weight, max_fails, fail_timeout, max_conns, server_status):
        payload = {
            "upstream": us_name,
            "operation": "modify",
            "type": "server",
            "host": None,
            "port": None,
            "changes": dict()
        }
        if server_status == "primary":
            payload["changes"]["backup"] = False
        elif server_status == "backup":
            payload["changes"]["backup"] = True
        state = None
        ch_list = {
            "weight": weight,
            "max_fails": max_fails,
            "fail_timeout": fail_timeout,
            "max_conns": max_conns
        }
        for k, v in ch_list.iteritems():
            if v not in ["none"]:
                payload["changes"][k] = int(v)
        for server in servers:
            addr, port = server.split(":")
            payload["host"] = addr
            payload["port"] = int(port)
            d = self.send_request_all_server(self.cfg["upstream_control_port"], self.cfg["upstream_control_endpoint"], payload)
            if d is not None:
                state = d
        if state is not None:
            return self.response_create(json.dumps({"STATUS": "error", "ERROR": state}))
        return self.response_create(json.dumps({"STATUS": "OK", "MESSAGE": "Server(s) properties is changed."}))

    @dynamic_us
    def add_us_server(self, server_ip, server_port, us_name, weight, max_fails, fail_timeout, max_conns, server_status):
        payload = {
            "upstream": us_name,
            "operation": "add",
            "host": server_ip,
            "port": int(server_port),
            "extras": dict()
        }
        if server_status == "backup":
            payload["extras"]["backup"] = True
        else:
            payload["extras"]["backup"] = False
        ch_list = {
            "weight": weight,
            "max_fails": max_fails,
            "fail_timeout": fail_timeout,
            "max_conns": max_conns
        }
        for k, v in ch_list.iteritems():
            if v not in ["none"]:
                payload["extras"][k] = int(v)
        d = self.send_request_all_server(self.cfg["upstream_control_port"], self.cfg["upstream_control_endpoint"], payload)
        if d is not None:
            return self.response_create(json.dumps({"STATUS": "error", "ERROR": d}))
        return self.response_create(json.dumps({"STATUS": "OK", "MESSAGE": "Server added to upstream."}))
