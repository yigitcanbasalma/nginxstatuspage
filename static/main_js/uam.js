// Main process file

var aPs = (function() {
    var data = [];
    var msec = {
        last: undefined,
        period: undefined
    };
    return {
        getValue: function(key, value) {
            if (typeof data[key] === 'undefined') {
                data[key] = value;
                return 'N/A';
            } else {
                var increase = value - data[key];
                data[key] = value;
                return Math.floor(increase * 1000 / msec.period);
            }
        },
        refresh: function(time) {
            msec.period = time - msec.last;
            msec.last = time;
        }
    };
})();

function mTh(msec) {
    var ms = 1000, m = 60, h = m * m, d = h * 24, s = '';
    if (msec < ms) {
        return msec + 'ms';
    }
    if (msec < (ms * m)) {
        return Math.floor(msec / ms) + '.' + Math.floor((msec % ms) / 10) + 's';
    }
    var days = Math.floor(msec / (d * ms));
    if (days) {
        s += days + "d ";
    }
    var hours = Math.floor((msec % (d * ms)) / (h * ms));
    if (days || hours) {
        s += hours + "h ";
    }
    var minutes = Math.floor(((msec % (d * ms)) % (h * ms)) / (m * ms));
    if (days || hours || minutes) {
        s += minutes + "m ";
    }
    var seconds = Math.floor((((msec % (d * ms)) % (h * ms)) % (m * ms)) / ms);
    return s + seconds + "s";
}

function bTh(b) {
    var kb = 1024;
    if (typeof b !== 'number') {
        return b;
    }
    if (b < kb) {
        return b + ' B';
    }
    if (b < (kb * kb)) {
        return (b / kb).toFixed(1) + ' KiB';
    }
    if (b < (kb * kb * kb)) {
        return (b / (kb * kb)).toFixed(1) + ' MiB';
    }
    return (b / (kb * kb * kb)).toFixed(2) + ' GiB';
}

function calculate_in_out_traffic(x) {
    var total_in = 0;
    var total_out = 0;
    $.each(x, function(k, v) {
        if (k == "*") {
           total_in = v.inBytes;
           total_out = v.outBytes;
        }
    });
    return [total_in, total_out];
}

function count_zone_summary_data(x) {
    var total_zone = 0;
    var total_incident = 0;
    $.each(x, function(k, v) {
        if (k != "*") {
           total_zone++;
           $.each(x[k].responses, function(e, s) {
               if (e == "5xx") {
                   if (aPs.getValue("zone_summary_5xx"+k, s) > 0) {
                       total_incident++;
                   }
               }
           });
        }
    });
    return [total_zone, total_incident];
}

function count_upstream_summary_data(x) {
    var total_upstream = 0;
    var total_incident = 0;
    var total_upstream_server = 0;
    var total_upstream_server_up = 0;
    var total_upstream_server_down = 0;
    var full_down = false;
    $.each(x, function(k, v) {
        total_upstream++;
        var cluster_info = {length: x[k].length, down: 0};
        $.each(x[k], function(e, s) {
            total_upstream_server++;
            $.each(k[e].responses, function(t, y) {
                if (t == "5xx") {
                    if (aPs.getValue(k, y) > 0) {
                        total_incident++;
                    }
                }
            });
            if (s.down) {
                total_upstream_server_down++;
                cluster_info.down += 1;
            } else {
                total_upstream_server_up++;
            }
        });
        if (cluster_info.length == cluster_info.down) {
            full_down = true;
        }
    });
    return [total_upstream, total_incident, total_upstream_server, total_upstream_server_up, total_upstream_server_down, full_down];
}

function placement_l7_zones(x) {
    var row = "";
    $.each(x, function(k, v) {
        if (k != "*") {
           var total_response_code = 0;
           var zone_status = "";
           $.each(v.responses, function(e, s) {
               if ((e == "1xx") || (e == "2xx") || (e == "3xx") || (e == "4xx") || (e == "5xx")) {
                   total_response_code += s
                   if (e == "5xx") {
                       if (aPs.getValue("l7_server_zones_5xx_err"+k, s) > 0) {
                           zone_status += "/static/images/Red_Cross.png";
                       } else {
                           zone_status += "/static/images/check.png";
                       }
                   }
               }
           });
           row += "<tr>";
           row += "     <td style='text-align: left;'>"+k+"<img style='float: right;' height='20px' src='"+zone_status+"'></td>";
           row += "     <td>"+v.requestCounter+"</td>";
           row += "     <td>"+aPs.getValue("pl_l7_zones"+k, v.requestCounter)+"</td>";
           row += "     <td>"+mTh(v.requestMsec)+"</td>";
           row += "     <td>"+v.responses["1xx"]+"</td>";
           row += "     <td>"+v.responses["2xx"]+"</td>";
           row += "     <td>"+v.responses["3xx"]+"</td>";
           row += "     <td>"+v.responses["4xx"]+"</td>";
           row += "     <td>"+v.responses["5xx"]+"</td>";
           row += "     <td>"+total_response_code+"</td>";
           row += "     <td>"+bTh(aPs.getValue("l7_server_zones_sent"+k, v.outBytes))+"</td>";
           row += "     <td>"+bTh(aPs.getValue("l7_server_zones_rcvd"+k, v.inBytes))+"</td>";
           row += "     <td>"+bTh(v.outBytes)+"</td>";
           row += "     <td>"+bTh(v.inBytes)+"</td>";
           row += "</tr>";
        }
    });
    if (row == "") {
        row += "<tr><td colspan='14' style='text-align: center;'>No zone found.</td></tr>"
    }
    $("#zones_table tbody tr").remove();
    $(row).appendTo("#zones_table tbody");
}

function upstream_table_template(x, y, z) {
    var html = "";
    if (x == "l7") {
        html += "<table class='display compact upstreams-table' style='width:100%' id='l7_upstream_options_"+y+"'>";
        html += "       <caption><label>"+y+"</label>&nbsp;&nbsp;&nbsp;<label><a onclick=\"server_status_change('l7_upstream_options_"+y+"', '"+y+"', 'enable')\" class='btn btn-default l7_upstream_options' disabled='disabled' href='#?' role='button'>Enable Server</a></label>&nbsp;<label><a onclick=\"server_status_change('l7_upstream_options_"+y+"', '"+y+"', 'disable')\" class='btn btn-default l7_upstream_options' disabled='disabled' href='#?' role='button'>Disable Server</a></label>&nbsp;<label><a onclick=\"server_edit('l7_upstream_options_"+y+"', '"+y+"')\" class='btn btn-default l7_upstream_options' disabled='disabled' href='#?' role='button'>Edit Server</a></label>&nbsp;<label><a onclick=\"new_server('"+y+"')\" class='btn btn-default' href='#?' role='button'>Add Server</a></label></caption>";
        html += "       <colgroup span='4'></colgroup>";
        html += "       <colgroup span='3'></colgroup>";
        html += "       <colgroup span='6'></colgroup>";
        html += "       <colgroup span='4'></colgroup>";
        html += "       <thead>";
        html += "           <tr>";
        html += "               <th scope='colgroup' colspan='1'>Address</th>";
        html += "               <th scope='colgroup' colspan='4'>General</th>";
        html += "               <th scope='colgroup' colspan='3'>Requests</th>";
        html += "               <th scope='colgroup' colspan='6'>Responses</th>";
        html += "               <th scope='colgroup' colspan='4'>Traffic</th>";
        html += "           </tr>";
        html += "           <tr class='sub-headers'>";
        html += "               <td scope='col'></td>";
        html += "               <td scope='col'>State</td>";
        html += "               <td scope='col'>Weight</td>";
        html += "               <td scope='col'>Max Fails</td>";
        html += "               <td scope='col'>Fail Timeout</td>";
        html += "               <td scope='col'>Req/s</td>";
        html += "               <td scope='col'>Time</td>";
        html += "               <td scope='col'>Total</td>";
        html += "               <td scope='col'>1xx</td>";
        html += "               <td scope='col'>2xx</td>";
        html += "               <td scope='col'>3xx</td>";
        html += "               <td scope='col'>4xx</td>";
        html += "               <td scope='col'>5xx</td>";
        html += "               <td scope='col'>Total</td>";
        html += "               <td scope='col'>Sent</td>";
        html += "               <td scope='col'>Rcvd</td>";
        html += "               <td scope='col'>Sent/s</td>";
        html += "               <td scope='col'>Rcvd/s</td>";
        html += "           </tr>";
        html += "       </thead>";
        html += "       <tbody>";
        html += "       </tbody>";
        html += "<table>";
    } else {
        html += "<table class='display compact tcp-upstreams-table' style='width:100%' id='l4_upstream_options_"+y+"'>";
        html += "       <caption><label>"+y+"</label>&nbsp;&nbsp;&nbsp;<label><a onclick=\"server_status_change('l4_upstream_options_"+y+"', '"+y+"', 'enable')\" class='btn btn-default l4_upstream_options' disabled='disabled' href='#' role='button'>Enable Server</a></label>&nbsp;<label><a onclick=\"server_status_change('l4_upstream_options_"+y+"', '"+y+"', 'disable')\" class='btn btn-default l4_upstream_options' disabled='disabled' href='#' role='button'>Disable Server</a></label>&nbsp;<label><a onclick=\"server_edit('l4_upstream_options_"+y+"', '"+y+"')\" class='btn btn-default l4_upstream_options' disabled='disabled' href='#' role='button'>Edit Server</a></label>&nbsp;<label><a onclick=\"new_server('"+y+"')\" class='btn btn-default' href='#' role='button'>Add Server</a></label></caption>";
        html += "       <colgroup span='4'></colgroup>";
        html += "       <colgroup span='3'></colgroup>";
        html += "       <colgroup span='3'></colgroup>";
        html += "       <colgroup span='6'></colgroup>";
        html += "       <colgroup span='4'></colgroup>";
        html += "       <thead>";
        html += "           <tr>";
        html += "               <th scope='colgroup' colspan='1'>Address</th>";
        html += "               <th scope='colgroup' colspan='4'>General</th>";
        html += "               <th scope='colgroup' colspan='3'>Response Time</th>";
        html += "               <th scope='colgroup' colspan='3'>Requests</th>";
        html += "               <th scope='colgroup' colspan='6'>Responses</th>";
        html += "               <th scope='colgroup' colspan='4'>Traffic</th>";
        html += "           </tr>";
        html += "           <tr class='sub-headers'>";
        html += "               <td scope='col'></td>";
        html += "               <td scope='col'>State</td>";
        html += "               <td scope='col'>Weight</td>";
        html += "               <td scope='col'>Max Fails</td>";
        html += "               <td scope='col'>Fail Timeout</td>";
        html += "               <td scope='col'>Connect</td>";
        html += "               <td scope='col'>First Byte</td>";
        html += "               <td scope='col'>Response</td>";
        html += "               <td scope='col'>Req/s</td>";
        html += "               <td scope='col'>Time</td>";
        html += "               <td scope='col'>Total</td>";
        html += "               <td scope='col'>1xx</td>";
        html += "               <td scope='col'>2xx</td>";
        html += "               <td scope='col'>3xx</td>";
        html += "               <td scope='col'>4xx</td>";
        html += "               <td scope='col'>5xx</td>";
        html += "               <td scope='col'>Total</td>";
        html += "               <td scope='col'>Sent</td>";
        html += "               <td scope='col'>Rcvd</td>";
        html += "               <td scope='col'>Sent/s</td>";
        html += "               <td scope='col'>Rcvd/s</td>";
        html += "           </tr>";
        html += "       </thead>";
        html += "       <tbody>";
        html += "       </tbody>";
        html += "<table>";
    }
    $(html).appendTo("#"+z);
}

function placement_l7_upstreams(x) {
    $.each(x, function(k, v) {
        var table_id = "l7_upstream_options_"+k+"";
        var html = "";
        if ($("#"+table_id).length == 0) {
            upstream_table_template("l7", k, "upstreams_table_area");
        }
        for (var j in v) {
            var i = v[j];
            var total_response_code = 0;
            var server_status = "";
            var server_state = "";
            var server_identifier = $().crypt({method: "md5", source: i.server});
            $.each(i.responses, function(e, s) {
                if ((e == "1xx") || (e == "2xx") || (e == "3xx") || (e == "4xx") || (e == "5xx")) {
                    total_response_code += s
                }
            });
            if (i.down) {
                server_status += "/static/images/down.png";
            } else {
                server_status += "/static/images/up.png";
            }
            if (i.backup) {
                server_state += "Backup";
            } else {
                server_state += "Primary";
            }
            if ($("#"+table_id+" tbody tr."+server_identifier).length == 0) {
                html += "<tr class='"+server_identifier+"'>";
                html += "     <td style='text-align: left;'><input value='"+i.server+"' onclick=\"enable_editing('"+table_id+"','l7_upstream_options')\" type='checkbox' class='custom-control-input'>&nbsp;&nbsp;"+i.server+"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img class='"+server_identifier+"_server_status' style='float: right;' height='20px' src='"+server_status+"'></td>";
                html += "     <td><label class='"+server_identifier+"_server_state'>"+server_state+"</label></td>";
                html += "     <td><label class='"+server_identifier+"_server_weight'>"+i.weight+"</label></td>";
                html += "     <td><label class='"+server_identifier+"_server_maxfails'>"+i.maxFails+"</label></td>";
                html += "     <td><label class='"+server_identifier+"_server_failtimeout'>"+i.failTimeout+"</label></td>";
                html += "     <td><label class='"+server_identifier+"_server_reqquest_per_second'>"+aPs.getValue("pl_l7_zones"+k, i.requestCounter)+"</label></td>";
                html += "     <td><label class='"+server_identifier+"_server_response_time'>"+mTh(i.responseMsec)+"</label></td>";
                html += "     <td><label class='"+server_identifier+"_server_total_req'>"+i.requestCounter+"</label></td>";
                html += "     <td><label class='"+server_identifier+"_server_1xx'>"+i.responses["1xx"]+"</label></td>";
                html += "     <td><label class='"+server_identifier+"_server_2xx'>"+i.responses["2xx"]+"</label></td>";
                html += "     <td><label class='"+server_identifier+"_server_3xx'>"+i.responses["3xx"]+"</label></td>";
                html += "     <td><label class='"+server_identifier+"_server_4xx'>"+i.responses["4xx"]+"</label></td>";
                html += "     <td><label class='"+server_identifier+"_server_5xx'>"+i.responses["5xx"]+"</label></td>";
                html += "     <td><label class='"+server_identifier+"_server_total_response_code'>"+total_response_code+"</label></td>";
                html += "     <td><label class='"+server_identifier+"_server_outbytes'>"+bTh(i.outBytes)+"</label></td>";
                html += "     <td><label class='"+server_identifier+"_server_inbytes'>"+bTh(i.inBytes)+"</label></td>";
                html += "     <td><label class='"+server_identifier+"_server_outbytes_per_second'>"+bTh(aPs.getValue("l7_server_zones_sent"+k, i.outBytes))+"</label></td>";
                html += "     <td><label class='"+server_identifier+"_server_inbytes_per_second'>"+bTh(aPs.getValue("l7_server_zones_rcvd"+k, i.inBytes))+"</label></td>";
                html += "</tr>";
            } else {
                $("#"+table_id+" ."+server_identifier+"_server_status").attr("src", server_status);
                $("#"+table_id+" ."+server_identifier+"_server_state").html(server_state);
                $("#"+table_id+" ."+server_identifier+"_server_weight").html(i.weight);
                $("#"+table_id+" ."+server_identifier+"_server_maxfails").html(i.maxFails);
                $("#"+table_id+" ."+server_identifier+"_server_failtimeout").html(i.failTimeout);
                $("#"+table_id+" ."+server_identifier+"_server_reqquest_per_second").html(aPs.getValue("pl_l7_zones"+k, i.requestCounter));
                $("#"+table_id+" ."+server_identifier+"_server_response_time").html(mTh(i.responseMsec));
                $("#"+table_id+" ."+server_identifier+"_server_total_req").html(i.requestCounter);
                $("#"+table_id+" ."+server_identifier+"_server_1xx").html(i.responses["1xx"]);
                $("#"+table_id+" ."+server_identifier+"_server_2xx").html(i.responses["2xx"]);
                $("#"+table_id+" ."+server_identifier+"_server_3xx").html(i.responses["3xx"]);
                $("#"+table_id+" ."+server_identifier+"_server_4xx").html(i.responses["4xx"]);
                $("#"+table_id+" ."+server_identifier+"_server_5xx").html(i.responses["5xx"]);
                $("#"+table_id+" ."+server_identifier+"_server_total_response_code").html(total_response_code);
                $("#"+table_id+" ."+server_identifier+"_server_outbytes").html(bTh(i.outBytes));
                $("#"+table_id+" ."+server_identifier+"_server_inbytes").html(bTh(i.inBytes));
                $("#"+table_id+" ."+server_identifier+"_server_outbytes_per_second").html(bTh(aPs.getValue("l7_server_zones_sent"+k, i.outBytes)));
                $("#"+table_id+" ."+server_identifier+"_server_inbytes_per_second").html(bTh(aPs.getValue("l7_server_zones_rcvd"+k, i.inBytes)));
            }
        }
        if (html != "") {
            $(html).appendTo("#"+table_id+" tbody");
            $("#"+table_id).DataTable();
        }
    });
}

function placement_l4_zones(x) {
    var row = "";
    $.each(x, function(k, v) {
        if (k != "*") {
           var total_response_code = 0;
           var zone_status = "";
           $.each(v.responses, function(e, s) {
               if ((e == "1xx") || (e == "2xx") || (e == "3xx") || (e == "4xx") || (e == "5xx")) {
                   total_response_code += s
                   if (e == "5xx") {
                       if (aPs.getValue("l4_server_zones_5xx_err"+k, s) > 0) {
                           zone_status += "/static/images/Red_Cross.png";
                       } else {
                           zone_status += "/static/images/check.png";
                       }
                   }
               }
           });
           row += "<tr>";
           row += "     <td style='text-align: left;'>"+k+"<img style='float: right;' height='20px' src='"+zone_status+"'></td>";
           row += "     <td>"+v.port+"</td>";
           row += "     <td>"+v.protocol+"</td>";
           row += "     <td>"+aPs.getValue("pl_l4_zones"+k, v.connectCounter)+"</td>";
           row += "     <td>"+mTh(v.requestMsec)+"</td>";
           row += "     <td>"+v.connectCounter+"</td>";
           row += "     <td>"+v.responses["1xx"]+"</td>";
           row += "     <td>"+v.responses["2xx"]+"</td>";
           row += "     <td>"+v.responses["3xx"]+"</td>";
           row += "     <td>"+v.responses["4xx"]+"</td>";
           row += "     <td>"+v.responses["5xx"]+"</td>";
           row += "     <td>"+total_response_code+"</td>";
           row += "     <td>"+bTh(v.outBytes)+"</td>";
           row += "     <td>"+bTh(v.inBytes)+"</td>";
           row += "     <td>"+bTh(aPs.getValue("l4_server_zones_sent"+k, v.outBytes))+"</td>";
           row += "     <td>"+bTh(aPs.getValue("l4_server_zones_rcvd"+k, v.inBytes))+"</td>";
           row += "</tr>";
        }
    });
    if (row == "") {
        row += "<tr><td colspan='16' style='text-align: center;'>No zone found.</td></tr>"
    }
    $("#tcp_udp_zones_table tbody tr").remove();
    $(row).appendTo("#tcp_udp_zones_table tbody");
}

function placement_l4_upstreams(x) {
    $.each(x, function(k, v) {
        var table_id = "l4_upstream_options_"+k+"";
        var html = "";
        if ($("#"+table_id).length == 0) {
            upstream_table_template("l4", k, "tcp_udp_upstreams_area");
        }
        for (var j in v) {
            var i = v[j];
            var total_response_code = 0;
            var server_status = "";
            var server_state = "";
            var server_identifier = $().crypt({method: "md5", source: i.server});
            $.each(i.responses, function(e, s) {
                if ((e == "1xx") || (e == "2xx") || (e == "3xx") || (e == "4xx") || (e == "5xx")) {
                    total_response_code += s
                }
            });
            if (i.down) {
                server_status += "/static/images/down.png";
            } else {
                server_status += "/static/images/up.png";
            }
            if (i.backup) {
                server_state += "Backup";
            } else {
                server_state += "Primary";
            }
            if ($("#"+table_id+" tbody tr."+server_identifier).length == 0) {
                html += "<tr class='"+server_identifier+"'>";
                html += "     <td style='text-align: left;'><input value='"+i.server+"' onclick=\"enable_editing('"+table_id+"','l4_upstream_options')\" type='checkbox' class='custom-control-input'>&nbsp;&nbsp;"+i.server+"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img class='"+server_identifier+"_server_status' style='float: right;' height='20px' src='"+server_status+"'></td>";
                html += "     <td><label class='"+server_identifier+"_server_state'>"+server_state+"</td>";
                html += "     <td><label class='"+server_identifier+"_server_weight'>"+i.weight+"</td>";
                html += "     <td><label class='"+server_identifier+"_server_maxfails'>"+i.maxFails+"</td>";
                html += "     <td><label class='"+server_identifier+"_server_failtimeout'>"+i.failTimeout+"</td>";
                html += "     <td><label class='"+server_identifier+"_server_connect_sec'>"+i.uConnectMsec+"</td>";
                html += "     <td><label class='"+server_identifier+"_server_firstbyte_sec'>"+i.uFirstByteMsec+"</td>";
                html += "     <td><label class='"+server_identifier+"_server_session_sec'>"+mTh(i.uSessionMsec)+"</td>";
                html += "     <td><label class='"+server_identifier+"_server_request_per_second'>"+aPs.getValue("pl_l4_zones"+k, i.connectCounter)+"</td>";
                html += "     <td><label class='"+server_identifier+"_server_session_sec'>"+mTh(i.sessionMsec)+"</td>";
                html += "     <td><label class='"+server_identifier+"_server_total_req'>"+i.connectCounter+"</td>";
                html += "     <td><label class='"+server_identifier+"_server_1xx'>"+i.responses["1xx"]+"</td>";
                html += "     <td><label class='"+server_identifier+"_server_2xx'>"+i.responses["2xx"]+"</td>";
                html += "     <td><label class='"+server_identifier+"_server_3xx'>"+i.responses["3xx"]+"</td>";
                html += "     <td><label class='"+server_identifier+"_server_4xx'>"+i.responses["4xx"]+"</td>";
                html += "     <td><label class='"+server_identifier+"_server_5xx'>"+i.responses["5xx"]+"</td>";
                html += "     <td><label class='"+server_identifier+"_server_total_response'>"+total_response_code+"</td>";
                html += "     <td><label class='"+server_identifier+"_server_outbytes'>"+bTh(i.outBytes)+"</td>";
                html += "     <td><label class='"+server_identifier+"_server_inbytes'>"+bTh(i.inBytes)+"</td>";
                html += "     <td><label class='"+server_identifier+"_server_outbytes_per_second'>"+bTh(aPs.getValue("l4_server_zones_sent"+k, i.outBytes))+"</td>";
                html += "     <td><label class='"+server_identifier+"_server_inbytes_per_second'>"+bTh(aPs.getValue("l4_server_zones_rcvd"+k, i.inBytes))+"</td>";
                html += "</tr>";
            } else {
                $("#"+table_id+" ."+server_identifier+"_server_status").attr("src", server_status);
                $("#"+table_id+" ."+server_identifier+"_server_state").html(server_state);
                $("#"+table_id+" ."+server_identifier+"_server_weight").html(i.weight);
                $("#"+table_id+" ."+server_identifier+"_server_maxfails").html(i.maxFails);
                $("#"+table_id+" ."+server_identifier+"_server_failtimeout").html(i.failTimeout);
                $("#"+table_id+" ."+server_identifier+"_server_connect_sec").html(i.uConnectMsec);
                $("#"+table_id+" ."+server_identifier+"_server_firstbyte_sec").html(i.uFirstByteMsec);
                $("#"+table_id+" ."+server_identifier+"_server_session_sec").html(mTh(i.uSessionMsec));
                $("#"+table_id+" ."+server_identifier+"_server_request_per_second").html(aPs.getValue("pl_l7_zones"+k, i.connectCounter));
                $("#"+table_id+" ."+server_identifier+"_server_session_sec").html(mTh(i.sessionMsec));
                $("#"+table_id+" ."+server_identifier+"_server_total_req").html(i.connectCounter);
                $("#"+table_id+" ."+server_identifier+"_server_1xx").html(i.responses["1xx"]);
                $("#"+table_id+" ."+server_identifier+"_server_2xx").html(i.responses["2xx"]);
                $("#"+table_id+" ."+server_identifier+"_server_3xx").html(i.responses["3xx"]);
                $("#"+table_id+" ."+server_identifier+"_server_4xx").html(i.responses["4xx"]);
                $("#"+table_id+" ."+server_identifier+"_server_5xx").html(i.responses["5xx"]);
                $("#"+table_id+" ."+server_identifier+"_server_total_response_code").html(total_response_code);
                $("#"+table_id+" ."+server_identifier+"_server_outbytes").html(bTh(i.outBytes));
                $("#"+table_id+" ."+server_identifier+"_server_inbytes").html(bTh(i.inBytes));
                $("#"+table_id+" ."+server_identifier+"_server_outbytes_per_second").html(bTh(aPs.getValue("l4_server_zones_sent"+k, i.outBytes)));
                $("#"+table_id+" ."+server_identifier+"_server_inbytes_per_second").html(bTh(aPs.getValue("l4_server_zones_rcvd"+k, i.inBytes)));
            }
        }
        if (html != "") {
            $(html).appendTo("#"+table_id+" tbody");
            $("#"+table_id).DataTable();
        }
    });
}

function placement_cache_zones(x) {
    var row = "";
    var main_status = "";
    $.each(x, function(k, v) {
        var total_response_code = 0;
        var zone_status = "";
        var warn_use = Math.round(v.maxSize * 70 / 100);
        var critical_use = Math.round(v.maxSize * 90 / 100);
        $.each(v.responses, function(e, s) {
            total_response_code += s;
        });
        if ((v.usedSize > warn_use) && (v.usedSize < critical_use)) {
            zone_status += "/static/images/warn.png";
            main_status += "/static/images/warn.png";
        } else if (v.usedSize > critical_use) {
            zone_status += "/static/images/Red_Cross.png";
            main_status += "/static/images/Red_Cross.png";
        } else {
            zone_status += "/static/images/check.png";
        }
        row += "<tr>";
        row += "     <td style='text-align: left;'>"+k+"<img style='float: right;' height='20px' src='"+zone_status+"'></td>";
        row += "     <td>"+bTh(v.maxSize)+"</td>";
        row += "     <td>"+bTh(v.usedSize)+"</td>";
        row += "     <td>"+bTh(v.outBytes)+"</td>";
        row += "     <td>"+bTh(v.inBytes)+"</td>";
        row += "     <td>"+bTh(aPs.getValue("l4_cache_zones_sent"+k, v.outBytes))+"</td>";
        row += "     <td>"+bTh(aPs.getValue("l4_cache_zones_rcvd"+k, v.inBytes))+"</td>";
        row += "     <td>"+v.responses["miss"]+"</td>";
        row += "     <td>"+v.responses["bypass"]+"</td>";
        row += "     <td>"+v.responses["expired"]+"</td>";
        row += "     <td>"+v.responses["stale"]+"</td>";
        row += "     <td>"+v.responses["updating"]+"</td>";
        row += "     <td>"+v.responses["revalidated"]+"</td>";
        row += "     <td>"+v.responses["hit"]+"</td>";
        row += "     <td>"+v.responses["scarce"]+"</td>";
        row += "     <td>"+total_response_code+"</td>";
        row += "</tr>";
    });
    if (row == "") {
        row += "<tr><td colspan='16' style='text-align: center;'>No cache found.</td></tr>";
    }
    if (main_status != "") {
        $("#caches_img").attr("src", main_status);
    } else {
        $("#caches_img").attr("src", "/static/images/check.png");
    }
    $("#cache_zone_table tbody tr").remove();
    $(row).appendTo("#cache_zone_table tbody");
}

function create_components(x) {
    // Define
    aPs.refresh(x.nowMsec)
    // L7 info side
    var l7_info = count_zone_summary_data(x.serverZones);
    var l7_traffic = calculate_in_out_traffic(x.serverZones);
    var l7_upstreams_info = count_upstream_summary_data(x.upstreamZones);
    // L4 info side
    var l4_info = count_zone_summary_data(x.streamServerZones)
    var l4_traffic = calculate_in_out_traffic(x.streamServerZones);
    var l4_upstreams_info = count_upstream_summary_data(x.streamUpstreamZones);
    // Left side
    $("#version").text(x.nginxVersion);
    $("#hostname").text(x.hostName);
    $("#uptime").text(mTh(x.nowMsec - x.loadMsec));
    if (x.state == "Master") {
        $("#role").text(x.state);
        $("#role").css("color", "green");
    } else {
        $("#role").text(x.state);
        $("#role").css("color", "red");
    }
    // Middle side
    $("#g_current").text(x.connections.active);
    $("#g_accept_per_second").text(aPs.getValue("g_accept_per_second", x.connections.accepted));
    $("#g_handled").text(x.connections.handled);
    $("#g_waiting").text(x.connections.waiting);
    $("#g_reading").text(x.connections.reading);
    $("#accepted").text(x.connections.accepted);
    // Right side
    $("#total_req").text(x.connections.requests);
    $("#r_per_second").text(aPs.getValue("r_per_second", x.connections.requests));
    // L7 side
    //// Zones side
    $("#l7_total_zones").text(l7_info[0]);
    $("#l7_zones_incidents").text(l7_info[1]);
    if (l7_info[1] > 0) {
        $("#l7_zones_incidents").css("color", "red");
        $("#server_zones_img").attr("src", "/static/images/Red_Cross.png");
    } else {
        $("#l7_zones_incidents").css("color", "green");
        $("#server_zones_img").attr("src", "/static/images/check.png");
    }
    $("#l7_traffic_in").text(bTh(aPs.getValue("l7_traffic_in", l7_traffic[0])));
    $("#l7_traffic_out").text(bTh(aPs.getValue("l7_traffic_out", l7_traffic[1])));
    //// Upstreams side
    $("#l7_upstreams").text(l7_upstreams_info[0]);
    $("#l7_upstream_incidents").text(l7_upstreams_info[1]);
    if (l7_upstreams_info[1] > 0) {
        $("#l7_upstream_incidents").css("color", "red");
    }else {
        $("#l7_upstream_incidents").css("color", "green");
    }
    $("#l7_upstream_servers").text(l7_upstreams_info[2]);
    $("#l7_upstream_up").text(l7_upstreams_info[3]);
    $("#l7_upstream_down").text(l7_upstreams_info[4]);
    if (l7_upstreams_info[5]){
        $("#l7_upstream_down_area").css("color", "red");
        $("#upstreams_img").attr("src", "/static/images/Red_Cross.png");
    }else if (l7_upstreams_info[4] > 0) {
        $("#l7_upstream_down_area").css("color", "red");
        $("#upstreams_img").attr("src", "/static/images/warn.png");
    } else {
        $("#l7_upstream_down_area").css("color", "green");
        $("#upstreams_img").attr("src", "/static/images/check.png");
    }
    // L4 side
    //// Zones side
    $("#l4_total_zones").text(l4_info[0]);
    $("#l4_zones_incidents").text(l4_info[1]);
    if (l4_info[1] > 0) {
        $("#l4_zones_incidents").css("color", "red");
        $("#tcp_udp_zones_img").attr("src", "/static/images/Red_Cross.png");
    } else {
        $("#l4_zones_incidents").css("color", "green");
        $("#tcp_udp_zones_img").attr("src", "/static/images/check.png");
    }
    $("#l4_traffic_in").text(bTh(aPs.getValue("l4_traffic_in", l4_traffic[0])));
    $("#l4_traffic_out").text(bTh(aPs.getValue("l4_traffic_out", l4_traffic[1])));
    //// Upstreams side
    $("#l4_upstreams").text(l4_upstreams_info[0]);
    $("#l4_upstream_incidents").text(l4_upstreams_info[1]);
    if (l4_upstreams_info[1] > 0) {
        $("#l4_upstream_incidents").css("color", "red");
    } else {
        $("#l4_upstream_incidents").css("color", "green");
    }
    $("#l4_upstream_servers").text(l4_upstreams_info[2]);
    $("#l4_upstream_up").text(l4_upstreams_info[3]);
    $("#l4_upstream_down").text(l4_upstreams_info[4]);
    if (l4_upstreams_info[5]) {
        $("#l4_upstream_down_area").css("color", "red");
        $("#tcp_udp_upstreams_img").attr("src", "/static/images/Red_Cross.png");
    }else if (l4_upstreams_info[4] > 0) {
        $("#l4_upstream_down_area").css("color", "red");
        $("#tcp_udp_upstreams_img").attr("src", "/static/images/warn.png");
    } else {
        $("#l4_upstream_down_area").css("color", "green");
        $("#tcp_udp_upstreams_img").attr("src", "/static/images/check.png");
    }
    // L7 zone table
    placement_l7_zones(x.serverZones);
    // L7 upstreams table
    placement_l7_upstreams(x.upstreamZones);
    // L4 zones table
    placement_l4_zones(x.streamServerZones);
    // L4 upstreams table
    placement_l4_upstreams(x.streamUpstreamZones);
    // Cache zone table
    placement_cache_zones(x.cacheZones)
}

function get_data() {
    // Ajax requester body
    $.ajax({
        type: 'GET',
        url: '/data',
        dataType: 'json',
        success: function(data) {
            if ( data.STATUS == "OK" ) {
                create_components(data.MESSAGE);
            } else {
                new PNotify({
                    title: 'Oops !!! Something went wrong.',
                    text: data.ERROR,
                    type: 'error'
                });
            }
        }
    });
}

function enable_editing(x,y) {
    var checked = document.querySelectorAll("#"+x+" tbody input[type='checkbox']:checked");
    var option_area = document.querySelectorAll("#"+x+" ."+y);
    if (checked.length > 0) {
        for (var i = 0; i < option_area.length; i++) {
            if (option_area[i].getAttribute("disabled") == "disabled") {
                option_area[i].removeAttribute("disabled");
            }
        }
    } else {
        for (var i = 0; i < option_area.length; i++) {
            if (option_area[i].getAttribute("disabled") != "disabled") {
                option_area[i].setAttribute("disabled", "disabled");
            }
        }
    }
}

function server_status_change(x,y,z) {
    var checked = document.querySelectorAll("#"+x+" tbody input[type='checkbox']:checked");
    var arr = [];
    if (checked.length > 0) {
        for (var i = 0; i < checked.length; i++) {
            arr.push(checked[i].value);
        }
        var data = JSON.stringify({"operation": z, "servers": arr, "upstream": y});
        // Ajax requester body
        $.ajax({
            type: 'POST',
            url: '/data',
            data: data,
            dataType: 'json',
            contentType: 'application/json;charset=UTF-8',
            success: function(data) {
                if ( data.STATUS == "OK" ) {
                    new PNotify({
                        title: 'Complete Successfully.',
                        text: data.MESSAGE,
                        type: 'success'
                    });
                } else {
                    new PNotify({
                        title: 'Oops !!! Something went wrong.',
                        text: data.ERROR,
                        type: 'error'
                    });
                }
            }
        });
    } else {
        new PNotify({
            title: 'Oops !!! Something went wrong.',
            text: 'Please select some server.',
            type: 'error'
        });
    }
}

function server_edit(x,y) {
    var checked = document.querySelectorAll("#"+x+" tbody input[type='checkbox']:checked");
    var arr = [];
    $("#const #us_name").remove();
    $("#const #servers").remove();
    if (checked.length > 0) {
        $("#modify_modal_us_name").html(y);
        if (checked.length == 1) {
            $("#modify_modal_server_name").html(checked[0].value);
            arr.push(checked[0].value);
        } else {
            for (var i = 0; i < checked.length; i++) {
                arr.push(checked[i].value);
            }
            $("#modify_modal_server_name").html(arr.join(", "));
        }
        $("<input id='us_name' value='"+y+"' />").appendTo("#const");
        $("<input id='servers' value='"+arr.join(",")+"' />").appendTo("#const");
        $("#modify_modal").modal("show");
    } else {
        new PNotify({
            title: 'Oops !!! Something went wrong.',
            text: 'Please select some server.',
            type: 'error'
        });
    }
}

function save_modified_changes() {
    var servers = $("#const #servers").val();
    var upstream = $("#const #us_name").val();
    var weight = $("#server_weight").val() || "none";
    var max_fails = $("#server_max_fails").val() || "none";
    var fail_timeout = $("#server_fail_timeout").val() || "none";
    var max_conns = $("#server_max_conns").val() || "none";
    var server_status = $("#server_status option:selected").val();
    var data = JSON.stringify({"operation": "modify", "servers": servers, "upstream": upstream, "weight": weight, "max_fails": max_fails, "fail_timeout": fail_timeout, "max_conns": max_conns, "server_status": server_status});
    // Ajax requester body
    $.ajax({
        type: 'POST',
        url: '/data',
        data: data,
        dataType: 'json',
        contentType: 'application/json;charset=UTF-8',
        success: function(data) {
            if ( data.STATUS == "OK" ) {
                new PNotify({
                    title: 'Complete Successfully.',
                    text: data.MESSAGE,
                    type: 'success'
                });
            } else {
                new PNotify({
                    title: 'Oops !!! Something went wrong.',
                    text: data.ERROR,
                    type: 'error'
                });
            }
        }
    });
}

function new_server(x) {
    $("#new_const #new_server_us_name").remove();
    $("<input id='new_server_us_name' value='"+x+"' />").appendTo("#new_const");
    $("#add_new_server_modal").modal("show");
}

function add_new_server() {
    var ip = $("#server_ip").val();
    var port = $("#server_port").val();
    var upstream = $("#new_const #new_server_us_name").val();
    var weight = $("#add_new_server_weight").val() || "none";
    var max_fails = $("#add_new_server_max_fails").val() || "none";
    var fail_timeout = $("#add_new_server_fail_timeout").val() || "none";
    var max_conns = $("#add_new_server_max_conns").val() || "none";
    var server_status = $("#add_new_server_status option:selected").val();
    var data = JSON.stringify({"operation": "add_new", "ip": ip, "port": port, "upstream": upstream, "weight": weight, "max_fails": max_fails, "fail_timeout": fail_timeout, "max_conns": max_conns, "server_status": server_status});
    // Ajax requester body
    $.ajax({
        type: 'POST',
        url: '/data',
        data: data,
        dataType: 'json',
        contentType: 'application/json;charset=UTF-8',
        success: function(data) {
            if ( data.STATUS == "OK" ) {
                new PNotify({
                    title: 'Complete Successfully.',
                    text: data.MESSAGE,
                    type: 'success'
                });
            } else {
                new PNotify({
                    title: 'Oops !!! Something went wrong.',
                    text: data.ERROR,
                    type: 'error'
                });
            }
        }
    });
}

setInterval(get_data, 1000);
//get_data();