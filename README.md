# NginxStatusPage

## Bağımlılıklar
* Python
	* requests
	* flask
* Nginx
	* nginx-module-vts
	* nginx-module-sts
	* nginx-dynamic-upstream
    
## Nginx Module Linkleri
* [Nginx Module VTS](https://github.com/vozlt/nginx-module-vts)
* [Nginx Module STS](https://github.com/vozlt/nginx-module-sts)
* [Nginx Module Dynamic Upstream](https://github.com/cubicdaiya/ngx_dynamic_upstream)
	
## Konfigürasyon Dosyası
* server_pool
	* master: Master sunucu IP adresi.
	* backup: Backup sunucu IP adresleri.
* status_endpoints
	* Bilgilerin çekileceği endpoint adresleri.
* sts_include
    * Sisteminizde stream server konfigürasyonu yoksa, bu alanı olduğu gibi bırakın.Eğer varsa "true" yapın.
* stream_server_replace
	* Stream server isim eşleştirmeleri.Key => "IP:PORT", Value => "SERVER-NAME".

```json
{
  "server_pool": {
    "master": "172.16.10.2",
    "backup": [
      "172.16.10.3"
    ]
  },
  "status_endpoints": [
    "/status/format/json",
    "/sstatus/format/json"
  ],
  "sts_include": false,
  "stream_server_replace": {
    "172.16.10.230:18001": "qa-rabbitmq"
  }
}
```

## Nginx Konfigürasyonu
Bu alanda anlatılan konfigürasyon, virtual host oluşturularak, istatistik bilginin çekilebileceği bir endpoint oluşturmaktır.
### nginx.conf
```bash
http {
    ...

    # Server all metric
    include     /etc/nginx/conf.d/default/http_vhost_traffic_zone.conf;
    
    ...
}
```

### http_vhost_traffic_zone.conf
```bash
vhost_traffic_status_zone shared:http_traffic_status:10m;
stream_server_traffic_status_zone;
vhost_traffic_status off;

server {
    access_log off;
    allow 127.0.0.1;
    allow 10.10.10.0/24; # Python uygulamasının çalışacağı IP
    deny all;

    location /status {
       vhost_traffic_status_display;
       vhost_traffic_status_display_format html;
    }

    location /sstatus {
        stream_server_traffic_status_display;
        stream_server_traffic_status_display_format html;
    }
}
```

## Ekran Görüntüleri
### Ana Ekran
![Main Screen](https://cdn.pbrd.co/images/HfuKPgn8.png)
### HTTP Zones
![HTTP Zones](https://cdn.pbrd.co/images/HfuMfOL.png)
### HTTP Upstreams
![HTTP Upstreams](https://cdn.pbrd.co/images/HfuMGQl.png)
### TCP/UDP Zones
![TCP/UDP Zones](https://cdn.pbrd.co/images/HfuN4PC.png)
### TCP/UDP Upstreams
![TCP/UDP Upstreams](https://cdn.pbrd.co/images/HfuNmuV.png)
### Cache Zones
![Cache Zones](https://cdn.pbrd.co/images/HfuNDM8.png)
